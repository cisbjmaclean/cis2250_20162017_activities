package info.hccis.cis2250.testactivities;

/**
 * Created by bjmaclean on 1/12/2017.
 */

public class Detail {

    private static String textEntered="0";

    public static String getTextEntered() {
        return textEntered;
    }

    public static void setTextEntered(String textEntered) {
        Detail.textEntered = textEntered;
    }
}
