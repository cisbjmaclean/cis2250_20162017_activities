package info.hccis.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AnalogClock;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.TimeZone;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentAndrew.OnFragmentAndrewInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentAndrew#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentAndrew extends Fragment implements RadioGroup.OnCheckedChangeListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private TextView timezoneTitle = null;
    private RadioGroup timezoneChoice = null;
    private RadioButton charlottetown = null;
    private RadioButton toronto= null;
    private RadioButton budapest= null;


    private OnFragmentAndrewInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Fragment3.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentAndrew newInstance(String param1, String param2) {
        FragmentAndrew fragment = new FragmentAndrew();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentAndrew() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_andrew, container, false);

        if(savedInstanceState!=null){
            configureViewInformation(savedInstanceState.getInt("timezoneChoice"));
            Log.d("ANDREWTEST", "in the if statement");
        } else {
            timezoneTitle = (TextView) view.findViewById(R.id.tvTimeZone);
            timezoneTitle.setText("Charlottetown");
            charlottetown = (RadioButton) view.findViewById(R.id.rbCharlottetown);
            charlottetown.setChecked(true);
            toronto= (RadioButton) view.findViewById(R.id.rbToronto);
            budapest= (RadioButton) view.findViewById(R.id.rbBudapest);
            timezoneChoice = (RadioGroup) view.findViewById(R.id.rgTimeZone);
            Log.d("ANDREWTEST", "in the else statement");
        }

        timezoneChoice.setOnCheckedChangeListener(this);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentAndrewInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentAndrewInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCheckedChanged(RadioGroup timezoneChoice, int checkedId) {
        configureViewInformation(checkedId);
    }

    private void configureViewInformation(int checkedId){
        if(checkedId==charlottetown.getId()){
            timezoneTitle.setText("Charlottetown");
            TimeZone.setDefault(TimeZone.getTimeZone("Canada/Atlantic"));
        } else if(checkedId==toronto.getId()){
            timezoneTitle.setText("Toronto");
            TimeZone.setDefault(TimeZone.getTimeZone("America/New_York"));
        } else if(checkedId==budapest.getId()){
            timezoneTitle.setText("Budapest");
            TimeZone.setDefault(TimeZone.getTimeZone("Europe/Budapest"));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState){
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("timezoneChoice", timezoneChoice.getCheckedRadioButtonId());
        Log.d("ANDREWTEST", "onSaveInstanceState() called");
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentAndrewInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentAndrewInteraction(String message);
    }

}
