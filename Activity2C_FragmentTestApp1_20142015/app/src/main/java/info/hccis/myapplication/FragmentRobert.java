package info.hccis.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentRobert.OnFragmentRobertInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentRobert#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentRobert extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private CountDownTimer countDownTimer;
    private boolean timerStarted = false;
    private Button buttonStart;
    public TextView textView;
    private long startTime = 60 * 1000;
    private long interval = 1 * 1000;

    private OnFragmentRobertInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentRobert.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentRobert newInstance(String param1, String param2) {
        FragmentRobert fragment = new FragmentRobert();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRobert(){
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_fragment_robert, container, false);
        View view = inflater.inflate(R.layout.fragment_fragment_robert, container, false);

        buttonStart = (Button) view.findViewById(R.id.button);
        textView = (TextView) view.findViewById(R.id.textView);
        countDownTimer = new CountDown(startTime, interval);
        textView.setText(textView.getText() + String.valueOf(startTime / 1000));
        buttonStart.setOnClickListener(new View.OnClickListener() {
            //Start and stop for timer
            @Override
            public void onClick(View v) {
                if (!timerStarted) {
                    countDownTimer.start();
                    timerStarted = true;
                    buttonStart.setText("STOP");
                    buttonStart.setBackgroundColor(Color.RED);
                } else {
                    countDownTimer.cancel();
                    timerStarted = false;
                    buttonStart.setText("RESTART");
                    buttonStart.setBackgroundColor(Color.MAGENTA);
                }
            }
        });
        return view;

    }
    public class CountDown extends CountDownTimer {
        public CountDown(long startTime, long interval) {
            super(startTime, interval);
        }


        public void onFinish() {
            textView.setText("The End");
        }

        @Override
        public void onTick(long millisUntilFinished) {
            textView.setText("" + millisUntilFinished / 1000);
        }

    }




    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentRobertInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentRobertInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentRobertInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentRobertInteraction(String message);
    }

}
