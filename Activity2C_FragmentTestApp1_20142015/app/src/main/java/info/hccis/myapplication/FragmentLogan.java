package info.hccis.myapplication;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentLogan.OnFragmentLoganInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentLogan#newInstance} factory method to
 * create an instance of this fragment.
 *
 * @author Logan Noonan
 * @since 2015-01-20
 *
 * This fragment contains examples of a switch and a toggle, types of compound buttons.
 * When triggered, this fragment will change text on the layout to reflect that the button
 * has been enabled/disabled.
 *
 */
public class FragmentLogan extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    private TextView switchStatus;
    private TextView toggleStatus;
    private Switch mySwitch;
    private ToggleButton myToggle;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentLoganInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentLogan.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentLogan newInstance(String param1, String param2) {
        FragmentLogan fragment = new FragmentLogan();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentLogan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /**
     * @author Logan Noonan
     * @since 2015-01-20
     *
     * This onCreateView method is run whenever the fragment is first instantiated. It attaches
     * the onClick listeners to the buttons and controls the display of the response messages.
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return view
     */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //inflate the fragment view
        View view = inflater.inflate(R.layout.fragment_fragment_logan, container, false);
        //match the switch and status string to the elements on the layout
        switchStatus = (TextView) view.findViewById(R.id.textView1);
        mySwitch = (Switch) view.findViewById(R.id.switch1);
        //attach the event listener to the switch
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //if the switch has been enabled
                    switchStatus.setText("Switch is ON");
                } else {
                    // The switch is disabled
                    switchStatus.setText("Switch is OFF");
                }
            }
        });

//        //match the toggle and status string to the elements on the layout
        toggleStatus = (TextView) view.findViewById(R.id.textView2);
        myToggle = (ToggleButton) view.findViewById(R.id.toggleButton1);
        //attach the event listener to the toggle
        myToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //if the toggle has been enabled
                    toggleStatus.setText("Toggle is ON");
                } else {
                    // The toggle is disabled
                    toggleStatus.setText("Toggle is OFF");
                }
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentLoganInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentLoganInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentLoganInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentLoganInteraction(String message);
    }

}
