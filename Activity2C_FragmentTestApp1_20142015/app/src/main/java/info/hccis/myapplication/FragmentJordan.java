package info.hccis.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.ToggleButton;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentJordan.OnFragmentJordanInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentJordan#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentJordan extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private TextView displayText = null;
    private RatingBar ratingBar2 = null;
    private ToggleButton toggleButton1 = null;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentJordanInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentJordan.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentJordan newInstance(String param1, String param2) {
        FragmentJordan fragment = new FragmentJordan();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentJordan() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fragment_jordan , container, false);
        return inflater.inflate(R.layout.fragment_fragment_jordan, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String message) {
        if (mListener != null) {
            mListener.onFragmentJordanInteraction(message);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentJordanInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void onStart(){
        super.onStart();

        displayText = (TextView) getActivity().findViewById(R.id.textView2);
        displayText.setText(String.valueOf(0));

        ratingBar2 = (RatingBar)getActivity().findViewById(R.id.ratingBar2);
        toggleButton1 = (ToggleButton)getActivity().findViewById(R.id.toggleButton);
        ratingBar2.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                updateText(String.valueOf(rating));
                Log.d("Rating",String.valueOf(rating));
            }
        });

        toggleButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ToggleButton toggleButton2 = (ToggleButton)v;
               String option =  toggleButton2.getText().toString();
               if(option.equalsIgnoreCase("On")){
                   ratingBar2.setVisibility(View.VISIBLE);
                   displayText.setVisibility(View.VISIBLE);
               }
                else if(option.equalsIgnoreCase("OFF")){
                   ratingBar2.setVisibility(View.INVISIBLE);
                   displayText.setVisibility(View.INVISIBLE);
               }
                else{
                   ratingBar2.setVisibility(View.VISIBLE);
                   displayText.setVisibility(View.VISIBLE);
               }
            }
        });

    }
    public void updateText(String rating){
        displayText.setText(rating);
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentJordanInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentJordanInteraction(String message);
    }

}
